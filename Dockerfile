FROM openjdk:8-alpine
ADD /build/libs/shopping-cart-0.1.0.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/app.jar"]