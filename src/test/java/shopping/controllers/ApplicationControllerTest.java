package shopping.controllers;

import org.junit.Test;
import shopping.controllers.models.response.HelloResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ApplicationControllerTest {

    @Test
    public void shouldReturnApplicationName() {
        ApplicationController applicationController = new ApplicationController();

        assertEquals(applicationController.getHello(), new HelloResponse());
    }
}