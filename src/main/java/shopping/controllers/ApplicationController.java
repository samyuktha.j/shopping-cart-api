package shopping.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shopping.controllers.models.response.HelloResponse;

@RestController
public class ApplicationController {

    @RequestMapping("/hello")
    public HelloResponse getHello() {
        return new HelloResponse();

    }
}
