package shopping.controllers.models.response;

import java.util.Objects;

public class HelloResponse {
    public String getValue() {
        return value;
    }

    String value = "Hello World";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HelloResponse that = (HelloResponse) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
